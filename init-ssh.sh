mkdir -p .ssh
rm -rf .ssh/id_rsa*
rm -rf .ssh/known_hosts
ssh-keygen -t rsa -b 4096 -C "git-sync" -f .ssh/id_rsa
ssh-keyscan -t rsa somehost.com >> .ssh/known_hosts