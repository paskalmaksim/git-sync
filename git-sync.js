const express = require('express');
const bodyParser = require('body-parser')
const app = express();
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const config = require('./config/index.json');
const fs = require('fs');
const path = require('path');
const { version } = require('./package.json');

app.use(bodyParser.json())

const validName = /^[a-zA-Z0-9_-]+$/;

const execute = async (line) => {
    try {
        const { stdout, stderr } = await exec(line);

        if (stdout) console.log(`[${new Date()}][${line}][stdout]${stdout}`);
        if (stderr) console.log(`[${new Date()}][${line}][stderr]${stderr}`);
    } catch (e) {
        console.error(`[${new Date()}][${line}][error]`, e);
    }
}

app.use('/git', express.static('public'));

app.get('/', (req, res) => {
    res.redirect('/git');
});

app.get('/git/api/version', (req, res) => {
    res.send(version);
});

app.get('/git/api/all', (req, res) => {
    res.type('json');
    let paths = [];
    config.repos.forEach(repo => {
        paths.push(repo.path);
    });
    res.send(JSON.stringify(paths));
});

app.use('/git/:name', async (req, res) => {
    const repo = req.params.name;

    if (!validName.test(req.params.name)) {
        return res.sendStatus(400);
    }
    
    const repoDir = path.join('./repo', repo);

    if (!fs.existsSync(repoDir)) {
        return res.sendStatus(404);
    }

    res.send('ok');

    console.log(`[${new Date()}][start ${repoDir}]`);

    await execute(`cd ${repoDir} && git fetch git_from --tags -p -P`);
    await execute(`cd ${repoDir} && git push git_to --tags`);
    await execute(`cd ${repoDir} && git push -f git_to "refs/remotes/git_from/*:refs/heads/*"`);
});

app.listen(config.server.port, () => {
    console.log(`Started server: ${JSON.stringify(config.server)}`);
}); 