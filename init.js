const config = require('./config/index.json');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const fs = require('fs');
const path = require('path');

const execute = async (line) => {
    try {
        const { stdout, stderr } = await exec(line);

        if (stdout) console.log(`[${line}][stdout]${stdout}`);
        if (stderr) console.log(`[${line}][stderr]${stderr}`);
    } catch (e) {
        console.error(`[${line}][error]`, e);
    }
}

const repoBase = './repo';

if (!fs.existsSync(repoBase)) {
    fs.mkdirSync(repoBase);
}

config.repos.forEach(async (repo) => {
    console.log(repo);

    const repoDir = path.join(repoBase, repo.path);

    console.log(repoDir);

    if (!fs.existsSync(repoDir)) {
        fs.mkdirSync(repoDir);
        await execute(`git clone ${repo.git_from} ${repoDir}`);

        await execute(`cd ${repoDir} && git remote remove origin`);
        await execute(`cd ${repoDir} && git remote add git_from ${repo.git_from}`);
        await execute(`cd ${repoDir} && git remote add git_to ${repo.git_to}`);
    }

    await execute(`cd ${repoDir} && git fetch --all -p`);
    await execute(`cd ${repoDir} && git push -f git_to "refs/remotes/git_from/*:refs/heads/*"`);
});